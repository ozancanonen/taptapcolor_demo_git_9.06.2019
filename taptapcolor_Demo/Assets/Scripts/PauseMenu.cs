﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour
{
    public static bool gameIsPaused;
    public GameObject pauseMenuUI;
    public GameObject endOfLevelMenuUI;
    public GameObject audioObject;//aduio manager game object
    private ButtonManager bm;
    public GameObject buttonManager;
    public GameObject gradeText;// the texts where we use on the end of level screen
    public GameObject highScoreText;
    public GameObject scoreText;
    public GameObject percentageText;

    private void Awake()
    {
        gameIsPaused = false;
        bm = buttonManager.GetComponent<ButtonManager>();
        highScoreText.GetComponent<Text>().text = "High Score:" + PlayerPrefs.GetFloat("HighScore").ToString();
        bm.nightMode = PlayerPrefs.GetInt("nightMode",0);

    }

    public void pasueModEnabled()
    {
        if (gameIsPaused)
        {
            Resume();
        }
        else
        {
            Pause();
        }
    }
    
    public void endOflLevelMenu()
    {
        if (bm.Score > PlayerPrefs.GetFloat("HighScore"))//we set new high score if the current score is higher than high score
        {
            PlayerPrefs.SetFloat("HighScore", bm.Score);
            highScoreText.GetComponent<Text>().text = "High Score:"+ bm.Score.ToString();
        }
        PlayerPrefs.SetInt("nightMode", bm.nightMode);
        gradeText.GetComponent<Text>().text= bm.Score.ToString();
        scoreText.GetComponent<Text>().text = bm.Score.ToString();
        percentageText.GetComponent<Text>().text = "%" + bm.ScorePercentage.ToString();
        audioObject.GetComponent<AudioManager>().pauseAll();//we pause all of the audios
        endOfLevelMenuUI.SetActive(true);
        Time.timeScale = 0;
    }
     void Pause()
        {
        audioObject.GetComponent<AudioManager>().pauseAll();
        pauseMenuUI.SetActive(true);
            Time.timeScale = 0;
            gameIsPaused = true;
        }
     void Resume()
        {
        audioObject.GetComponent<AudioManager>().playAll();
        pauseMenuUI.SetActive(false);
            Time.timeScale = 1;
            gameIsPaused = false;
        }

}

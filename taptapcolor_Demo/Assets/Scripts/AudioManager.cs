﻿using UnityEngine.Audio;
using System;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public Sound[] sounds;
    void Awake()
    {
        foreach(Sound s in sounds)
        {
           s.source= gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
        } 
    }

    private void Start()
    {
        Play("Bass");
        Play("EverythingXBass");
    }

    public void Play (string name)
    {
        Sound s= Array.Find(sounds, sound => sound.name == name);
        s.source.Play();
    }

    public void Pause(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        s.source.Pause();
    }

    public void volumeDown(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        s.source.volume = 0;
    }
    public void volumeNormal(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        s.source.volume = 2;
    }

    public void pauseAll()
    {
        Pause("Bass");
        Pause("EverythingXBass");
    }

    public void playAll()
    {
        Play("Bass");
        Play("EverythingXBass");
    }
}

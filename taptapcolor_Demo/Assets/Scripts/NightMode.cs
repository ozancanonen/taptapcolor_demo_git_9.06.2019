﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NightMode : MonoBehaviour
{
    public GameObject frontWall;
    public GameObject topWall;
    public GameObject bottomWall;
    public GameObject leftWall;
    public GameObject rightWall;
    public GameObject road;
    public GameObject thumbsUpp;
    public GameObject faultIndicator1;
    public GameObject faultIndicator2;
    public GameObject buttonManager;
    public GameObject text1;
    public GameObject text2;
    public GameObject text3;
    public GameObject colorBar;
    public GameObject colorBar2;

    private ButtonManager bm;

    void Start()
    {
        bm = buttonManager.GetComponent<ButtonManager>();
        changeColor();

    }

    public void changeColor()
    {
         if (bm.nightMode==0)
        {
            frontWall.GetComponent<MeshRenderer>().material = bm.grayMat;
            topWall.GetComponent<MeshRenderer>().material = bm.grayMat;
            bottomWall.GetComponent<MeshRenderer>().material = bm.grayMat;
            leftWall.GetComponent<MeshRenderer>().material = bm.grayMat;
            rightWall.GetComponent<MeshRenderer>().material = bm.grayMat;
            road.GetComponent<MeshRenderer>().material = bm.grayMat;
            thumbsUpp.GetComponent<SpriteRenderer>().color = Color.black;
            faultIndicator1.GetComponent<MeshRenderer>().material = bm.blackMat;
            faultIndicator2.GetComponent<MeshRenderer>().material = bm.blackMat;
            text1.GetComponent<TextMesh>().color = Color.black;
            text2.GetComponent<TextMesh>().color = Color.black;
            text3.GetComponent<TextMesh>().color = Color.black;
            colorBar.GetComponent<MeshRenderer>().material = bm.blackMat;
            colorBar2.GetComponent<MeshRenderer>().material = bm.grayMat;
        }
        else
        {
            frontWall.GetComponent<MeshRenderer>().material = bm.blackMat;
            topWall.GetComponent<MeshRenderer>().material = bm.blackMat;
            bottomWall.GetComponent<MeshRenderer>().material = bm.blackMat;
            leftWall.GetComponent<MeshRenderer>().material = bm.blackMat;
            rightWall.GetComponent<MeshRenderer>().material = bm.blackMat;
            road.GetComponent<MeshRenderer>().material = bm.blackMat;
            thumbsUpp.GetComponent<SpriteRenderer>().color = Color.white;
            faultIndicator1.GetComponent<MeshRenderer>().material = bm.whiteMat;
            faultIndicator2.GetComponent<MeshRenderer>().material = bm.whiteMat;
            text1.GetComponent<TextMesh>().color = Color.white;
            text2.GetComponent<TextMesh>().color = Color.white;
            text3.GetComponent<TextMesh>().color = Color.white;
            colorBar.GetComponent<MeshRenderer>().material = bm.grayMat;
            colorBar2.GetComponent<MeshRenderer>().material = bm.blackMat;
        }
    }
}

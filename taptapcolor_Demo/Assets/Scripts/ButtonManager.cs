﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonManager : MonoBehaviour
{
    [HideInInspector] public float Score;
    [HideInInspector] public float ScorePercentage;
    [HideInInspector] public string score;
    [HideInInspector] public string scorePercentage;
    [HideInInspector] public float totalNote; //number of plaer's touch to the screen
    [HideInInspector] public float multiplier;
    [HideInInspector] public float scoreInARow;
    [HideInInspector] public float succesfullNote;
    [HideInInspector] public GameObject currentNote; //current note game object(that is enterred the trigger)
    [HideInInspector] public int nightMode;
    public GameObject thumpsUp; //thumbs upp correct indicator
    public GameObject faultIndicator; // cross fault indicator
    public GameObject audioManager;
    public GameObject noteManager;
    public GameObject nightModeManager;
    public CameraShake cameraShake;

    public GameObject redLine; // the neon line
    public GameObject yellowLine;
    public GameObject blueLine;

    public Gradient colorBlue; //is used for lightning colors
    public Gradient colorRed;
    public Gradient colorYellow;
    public GameObject textObject; 
    public GameObject textObject2;
    public GameObject textObject3;
    public GameObject colorBar;
    //public GameObject explosion;
    //public GameObject explosionParent;
    public GameObject lightning;
    public GameObject lightningParent;
    public Transform blueLinePos; //the line positions which will be used for lightning begining pos
    public Transform redLinePos;
    public Transform yellowLinePos;
    public Transform lightningSpawnPos; //actually the middle pos between begining pos and end pos
    public Transform lighningEndPos;
    private Transform explosionSpawnPos;

    public Material redMat;//these are used for changing the bar's color
    public Material blueMat;
    public Material yellowMat;
    public Material purpleMat;
    public Material orangeMat;
    public Material greenMat;
    public Material blackMat;
    public Material whiteMat;
    public Material grayMat;

    [HideInInspector] public bool orangeActive;
    [HideInInspector] public bool greenActive;
    [HideInInspector] public bool purpleActive;

    [HideInInspector] public bool purpleScored;//contols if we have succesfully pressed the right buttons while the note is on the trigger zone
    [HideInInspector] public bool orangeScored;
    [HideInInspector] public bool greenScored;
    public SceneLoader sceneLoader;

    public GameObject blueButtonCs;
    public GameObject redButtonCs;
    public GameObject yellowButtonCs;
    private blueButton bB;
    private redButton rB;
    private yellowButton yB;

    void Start()
    {
        bB = blueButtonCs.GetComponent<blueButton>();
        rB = redButtonCs.GetComponent<redButton>();
        yB = yellowButtonCs.GetComponent<yellowButton>();
        score = textObject.GetComponent<TextMesh>().text;
        scorePercentage = textObject2.GetComponent<TextMesh>().text;
        Score = 0;
        ScorePercentage = 0;
        succesfullNote = 0;
        orangeActive = false;
        purpleActive = false;
        greenActive = false;
        orangeScored = false;
        purpleScored = false;
        greenScored = false;
        multiplier = 1;

    }
    public void AddScore()
    {
        scoreInARow++;
        if (scoreInARow>=10)
        {
            scoreInARow = 0;
            multiplier++;
        }
        succesfullNote++;
        Score = Score + multiplier;
        RefreshTexts();
    }
    public void DecreaseScore()
    {
        scoreInARow = 0;
        multiplier = 1;
        Score--;
        if (Score <= -10)
        {
            sceneLoader.LoseScene();
        }
        RefreshTexts();
    }
    public void ResetMultiplier()
    {
        scoreInARow = 0;
        multiplier = 1;
        RefreshTexts();
    }

    private void RefreshTexts()
    {
        ScorePercentage = succesfullNote / totalNote * 100;
        ScorePercentage = Mathf.Round(ScorePercentage);
        textObject.GetComponent<TextMesh>().text = Score.ToString();
        textObject2.GetComponent<TextMesh>().text = "%" + ScorePercentage.ToString();
        textObject3.GetComponent<TextMesh>().text = "x" + multiplier.ToString();
    }


    public void GreenCheck()
    {
        if (!greenScored && greenActive && yB.isPressed && bB.isPressed)
            {
            greenScored = true;
            greenActive = false;
            PlayerCorrectClickVisualiziation(greenMat);
            }
    }

    public void PurpleCheck()
    {
            if (!purpleScored && purpleActive && rB.isPressed && bB.isPressed)
            {
            purpleScored = true;
            purpleActive = false;
            PlayerCorrectClickVisualiziation(purpleMat);
        }
    }

    public void OrangeCheck()
    {
        if (!orangeScored && orangeActive && yB.isPressed && rB.isPressed)
            {
            orangeScored = true;
            orangeActive = false;
            PlayerCorrectClickVisualiziation(orangeMat);
        }
    }

    private void PlayerCorrectClickVisualiziation(Material Mat)
    {
        succesfullNote++;
        PlayerCorrectVolumeNormal();// the plater's instrumans volume is normal
        redLine.GetComponent<MeshRenderer>().material = Mat;
        blueLine.GetComponent<MeshRenderer>().material = Mat;
        yellowLine.GetComponent<MeshRenderer>().material = Mat;
        redLine.SetActive(true);
        blueLine.SetActive(true);
        yellowLine.SetActive(true);
        thumpsUp.SetActive(true);
        colorBar.GetComponent<MeshRenderer>().material = Mat;
        AddScore();
        Destroy(currentNote);
    }
    //public void instanitateExplosion()
    //{
    //    explosionSpawnPos = currentNote.transform;
    //    GameObject Explosion = Instantiate(explosion, explosionSpawnPos);
    //    Explosion.transform.parent = explosionParent.transform;
    //}

    public IEnumerator SpawnLightning() 
    {
        GameObject Lightning = Instantiate(lightning, blueLinePos);
        Lightning.transform.parent = lightningParent.transform;
        if (currentNote.tag == "blueNote")
        {
            Lightning.GetComponent<LineRenderer>().colorGradient = colorBlue;
            Lightning.transform.GetChild(0).transform.position = blueLinePos.position;
        }
        if (currentNote.tag == "redNote")
        {
            Lightning.GetComponent<LineRenderer>().colorGradient = colorRed;
            Lightning.transform.GetChild(0).transform.position = redLinePos.position;
        }
        if (currentNote.tag == "yellowNote")
        {
            Lightning.GetComponent<LineRenderer>().colorGradient = colorYellow;
            Lightning.transform.GetChild(0).transform.position = yellowLinePos.position;
        }
        Lightning.transform.GetChild(1).transform.position = currentNote.transform.position;
        yield return new WaitForSeconds(0.2f);
        Destroy(Lightning);
    }

    public void PlayerFaultVolumeDown()// makes the volume of the player's instuments equal to zero when the player fails to touch a note
    {
        audioManager.GetComponent<AudioManager>().volumeDown("Bass");
    }

    public void PlayerCorrectVolumeNormal() //makes the volume of the player's instuments equal to it's normal value when there is no fault
    {
        audioManager.GetComponent<AudioManager>().volumeNormal("Bass");
    }
    public void NightMode()
    {
        if (nightMode == 0)
        {
            nightMode = 1;
            nightModeManager.GetComponent<NightMode>().changeColor();
        }
        else
        {
            nightMode = 0;
            nightModeManager.GetComponent<NightMode>().changeColor();
        }
    }
}
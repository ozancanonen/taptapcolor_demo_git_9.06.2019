﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyer : MonoBehaviour
{
    public GameObject buttons;
    public GameObject noteManager;
    private NoteManager nm;
    private ButtonManager bm;
    public CameraShake cameraShake;

    private void Start()
    {
        bm=buttons.GetComponent<ButtonManager>();
        nm = noteManager.GetComponent<NoteManager>();
    }

    private void OnTriggerEnter(Collider col)
    {
        //if any notes enter the destroyer's zone 
        if (!nm.noteCreator&&(col.tag == "redNote"|| col.tag == "blueNote" || col.tag == "yellowNote"
            || col.tag == "orangeNote" || col.tag == "purpleNote" || col.tag == "greenNote"))
        {
            bm.DecreaseScore();
            StartCoroutine(faultIndicatorMakeItSeen());
            bm.PlayerFaultVolumeDown();
            Destroy(col.gameObject);
            StartCoroutine(cameraShake.Shake(0.05f, 0.02f));
        }
    }

    IEnumerator faultIndicatorMakeItSeen()
    {
        bm.faultIndicator.SetActive(true);
        yield return new WaitForSeconds(0.3f);
        bm.faultIndicator.SetActive(false);
    }
}

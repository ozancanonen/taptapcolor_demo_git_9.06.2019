﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class SceneLoader : MonoBehaviour
{
    public GameObject medium1Lighntning;
    public GameObject medium1;
    public GameObject medium2Lighntning;
    public GameObject medium2;
    public GameObject medium3Lighntning;
    public GameObject medium3;
    private bool onetime=false;
    public void LoadNextScene()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentSceneIndex + 1);
    }

    public void LoadStartScene()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }
    public void LoadEasyScene()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("Easy");
    }
    public void LoadMedium1Scene()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("Medium.1");
    }
    public void LoadMedium2Scene()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("Medium.2");
    }
    public void LoadMedium3Scene()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("Medium.3");
    }
    public void LoadHardScene()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("Hard");
    }

    public void QuitGame()
    {
        Application.Quit();
    }
    public void LoseScene()
    {
        SceneManager.LoadScene("Lose Screen");
    }
    public void Replay()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        Time.timeScale = 1;
    }
    public void MiddleButton()
    {
        if(!onetime)
        StartCoroutine(spawnLightningforMedium());
    }
    public IEnumerator spawnLightningforMedium()
    {
        onetime = true;
        medium1Lighntning.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        medium1.SetActive(true);
        medium1Lighntning.SetActive(false);
        yield return new WaitForSeconds(0.1f);
        medium2Lighntning.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        medium2Lighntning.SetActive(false);
        medium2.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        medium3Lighntning.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        medium3Lighntning.SetActive(false);
        medium3.SetActive(true);
    }
}

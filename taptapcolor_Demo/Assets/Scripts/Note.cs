﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Note : MonoBehaviour
{
    private ButtonManager bm;
    private float speed;
    void Start()
    {
        bm = GetComponentInParent<ButtonManager>();
        speed =GetComponentInParent<NoteManager>().noteSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position -= Time.deltaTime * transform.forward * speed;
    }
}

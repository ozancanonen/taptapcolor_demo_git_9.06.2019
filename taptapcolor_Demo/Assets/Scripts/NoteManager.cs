﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteManager : MonoBehaviour
{
    public GameObject[] cubes;
    public Transform spawnPos;
    public float beat;
    private float timer;
    private float totalBeat;
    private float beatCounter;
    public float noteSpeed;
    public bool noteCreator;
    public GameObject PauseMenu;
    PauseMenu pauseMenu;

    private void Start()
    {
        timer = -1.5f;//the track stars after 1.5 + beat seconds after
        totalBeat = /*beat * 3.9f;*/50; // indicates the number of notes that is going to spawned
        pauseMenu = PauseMenu.GetComponent<PauseMenu>();
    }
    void Update()
    {
        //spawns notes according to the beat value in inspector
        if (!noteCreator&&timer > beat&&beatCounter<= totalBeat)
        {
            beatCounter++;
            GameObject cube = Instantiate(cubes[Random.Range(0, cubes.Length)], spawnPos);
            cube.transform.parent = gameObject.transform;
            timer = 0;
        }
        timer += Time.deltaTime;

        //as we spawn the amount of notes requested we initilize the end of level sequence
        if (beatCounter >= totalBeat)
        {
            StartCoroutine(loadEndOfLevelMenuAfterFewSeconds());
        }
    }

    IEnumerator loadEndOfLevelMenuAfterFewSeconds()
    {
        yield return new WaitForSeconds(3f);
        pauseMenu.endOflLevelMenu();
        
    }
}

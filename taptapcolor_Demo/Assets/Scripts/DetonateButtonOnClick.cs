﻿using UnityEngine;
using UnityEngine.EventSystems;
public class DetonateButtonOnClick : MonoBehaviour, IPointerDownHandler,IPointerUpHandler// required interface when using the OnPointerDown method.
{
    public GameObject redButton;
    public GameObject blueButton;
    public GameObject yellowButton;
    private ButtonManager bm;

    void Start()
    {
        bm = redButton.GetComponentInParent<ButtonManager>();
    }
    //used for making the button alunch when pressed but not released
    public void OnPointerDown(PointerEventData eventData)
    {
        if (gameObject.name == "clickableRedButton")
            redButton.GetComponent<redButton>().ButtonFonction(ref bm.orangeScored,ref bm.purpleScored);
        if (gameObject.name == "clickableBlueButton")
            blueButton.GetComponent<blueButton>().ButtonFonction(ref bm.purpleScored,ref bm.greenScored);
        if (gameObject.name == "clickableYellowButton")
            yellowButton.GetComponent<yellowButton>().ButtonFonction(ref bm.greenScored,ref bm.orangeScored);
    }
    public void OnPointerUp(PointerEventData eventData)
    {

        if (redButton.GetComponent<redButton>().isPressing&& redButton.GetComponent<redButton>().isReleasable) {

            if (gameObject.name == "clickableRedButton")
                redButton.GetComponent<redButton>().ButtonFonction(ref bm.orangeScored, ref bm.purpleScored);
            if (gameObject.name == "clickableBlueButton")
                blueButton.GetComponent<blueButton>().ButtonFonction(ref bm.purpleScored, ref bm.greenScored);
            if (gameObject.name == "clickableYellowButton") ;
            yellowButton.GetComponent<yellowButton>().ButtonFonction(ref bm.greenScored, ref bm.orangeScored);
        }
        else
        {
            redButton.GetComponent<redButton>().madeAFault();
        }
    }

}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class blueButton : ColorButton
{

    public override void Start()
    {
        base.Start();
        mainNote = "blueNote";
        sideNote1 = "purpleNote";
        sideNote2 = "greenNote";
        colorMat = bm.blueMat;
        colorLine = bm.blueLine;
        
        //button.onClick.AddListener(() =>
        //{
        //    ButtonFonction(bm.bluePressed,bm.purpleScored,bm.greenScored,bm.blueActive);
        //});
    }
    //public override void OnPointerDown(PointerEventData eventData)
    //{
    //    ButtonFonction(bm.bluePressed, bm.purpleScored, bm.greenScored, bm.blueActive);
    //}
    public override void OnTriggerEnter(Collider col)
    {
        whenNoteIsNearby(col,ref bm.purpleScored,ref bm.greenScored,ref bm.purpleActive);
    }
    public override void OnTriggerExit(Collider col)
    {
        whenNoteExits(col);
    }
    public override void SideColor1Check()
    {
        bm.PurpleCheck();
    }
    public override void SideColor2Check()
    {
        bm.GreenCheck();
    }
    public override bool IfSideColorsScored()
    {
        if (bm.greenScored || bm.purpleScored)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public abstract class ColorButton : MonoBehaviour
{
    public GameObject note4Button;
    public Button button;
    public Transform spawnPos;

    protected ButtonManager bm;
    protected NoteManager nm;
    protected GameObject colorLine;
    protected Material colorMat;
    protected string mainNote;
    protected string longMainNoteEnd;
    protected string sideNote1;
    protected string sideNote2;
    private Vector3 normalPos;
    private Vector3 clickedPos;
    private float counter;
    private int noteCheck;
    [HideInInspector] public bool isActive;
    [HideInInspector] public bool isPressed;
    [HideInInspector] public bool isPressing;
    [HideInInspector] public bool isReleasable;
    [HideInInspector] public bool longNote;

    public virtual void Start()
    {
        bm = GetComponentInParent<ButtonManager>();
        nm = bm.noteManager.GetComponent<NoteManager>();
        normalPos = gameObject.transform.position;
        clickedPos = normalPos - new Vector3(0f, 0.05f, 0f);
        counter = 0;
        longNote = false;
    }

    public virtual void ButtonFonction(ref bool sideColor1Scored,ref bool sideColor2Scored)
    {
        bm.totalNote++;
        gameObject.transform.position = clickedPos;
        StartCoroutine(Pressed());
        if (nm.noteCreator)
        {
            GameObject cube = Instantiate(note4Button, spawnPos);
            cube.transform.parent = bm.noteManager.transform;
        }
        else
        {
            while (counter <= 0.1f && noteCheck == 0 && !sideColor2Scored && !sideColor1Scored)
            {
                counter = counter + Time.deltaTime;
                if (bm.currentNote != null && bm.currentNote.tag == sideNote1)
                {
                    SideColor1Check();
                }
                if (bm.currentNote != null && bm.currentNote.tag == sideNote2)
                {
                    SideColor2Check();
                }
                else if (isActive&& bm.currentNote != null && bm.currentNote.tag == mainNote)
                {
                    bm.colorBar.GetComponent<MeshRenderer>().material = colorMat;
                    colorLine.GetComponent<MeshRenderer>().material = colorMat;
                    colorLine.SetActive(true);
                    bm.thumpsUp.SetActive(true);
                    bm.PlayerCorrectVolumeNormal();
                    noteCheck++;
                                        //bm.instanitateExplosion();
                    //StartCoroutine(bm.spawnLightning());
                    if (bm.currentNote != null && bm.currentNote.transform.GetChild(0) != null&&!isPressing)
                    {
                        longNote = true;
                        isPressing = true;
                    }
                    else
                    {
                        bm.AddScore();
                        Destroy(bm.currentNote);
                    }
                }
            }
        }
    }

    IEnumerator Pressed()
    {
        isPressed = true;
        yield return new WaitForSecondsRealtime(0.11f);
        gameObject.transform.position = normalPos;
        if (noteCheck == 0 && IfSideColorsScored() && !longNote)
        {
            madeAFault();
        }
        if (longNote)
        {
            
        }
        else
        {
            noteCheck = 0;
            counter = 0;
            isPressed = false;
            yield return new WaitForSecondsRealtime(0.2f);
            bm.redLine.SetActive(false);
            bm.blueLine.SetActive(false);
            bm.yellowLine.SetActive(false);
            bm.thumpsUp.SetActive(false);
            bm.faultIndicator.SetActive(false);
            if (bm.nightMode == 0)
                bm.colorBar.GetComponent<MeshRenderer>().material = bm.blackMat;
            else
                bm.colorBar.GetComponent<MeshRenderer>().material = bm.whiteMat;
        }
    }
    public void madeAFault()
    {
        bm.DecreaseScore();
        bm.faultIndicator.SetActive(true);
        bm.PlayerFaultVolumeDown();
        StartCoroutine(bm.cameraShake.Shake(0.05f, 0.02f));
    }
    public virtual bool IfSideColorsScored ()
    {
        return false;
    }

    public virtual void SideColor1Check()
    {

    }
    public virtual void SideColor2Check()
    {

    }


    public abstract void OnTriggerEnter(Collider col);

    public virtual void whenNoteIsNearby(Collider col,ref bool sideColor1Scored,ref bool sideColor2Scored,ref bool sideColor1Active)
    {
        if (col.gameObject.tag == mainNote || col.gameObject.tag == sideNote2)
        {
            isActive = true;
            bm.currentNote = col.gameObject;
            sideColor1Scored = false;
            sideColor2Scored = false;
        }
        if (col.gameObject.tag == sideNote1)
        {
            sideColor1Active = true;
            bm.currentNote = col.gameObject;
            sideColor1Scored = false;
            sideColor2Scored = false;
        }
        if (col.gameObject.tag == longMainNoteEnd)
        {
            isReleasable = true;
        }

    }
    public abstract void OnTriggerExit(Collider col);

    public virtual void whenNoteExits(Collider col)
    {
        if (col.gameObject.tag == mainNote || col.gameObject.tag == sideNote1 || col.gameObject.tag == sideNote2)
        {
            isActive = false;
        }
        if (col.gameObject.tag == longMainNoteEnd)
        {
            isReleasable = false;
        }
    }

}

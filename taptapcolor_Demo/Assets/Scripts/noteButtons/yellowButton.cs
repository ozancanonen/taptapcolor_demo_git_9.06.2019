﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class yellowButton : ColorButton
{

    public override void Start()
    {
        base.Start();
        mainNote = "yellowNote";
        sideNote1 = "greenNote";
        sideNote2 = "orangeNote";
        colorMat = bm.yellowMat;
        colorLine = bm.yellowLine;

        //button.onClick.AddListener(() =>
        //{
        //    ButtonFonction(bm.yellowPressed,bm.greenScored,bm.orangeScored,bm.yellowActive);
        //});
    }
    //public override void OnPointerDown(PointerEventData eventData)
    //{
    //    ButtonFonction(bm.yellowPressed, bm.greenScored, bm.orangeScored, bm.yellowActive);
    //}
    public override void OnTriggerEnter(Collider col)
    {
        whenNoteIsNearby(col,ref bm.greenScored,ref bm.orangeScored,ref bm.greenActive);
    }
    public override void OnTriggerExit(Collider col)
    {
        whenNoteExits(col);
    }
    public override void SideColor1Check()
    {
        bm.GreenCheck();
    }
    public override void SideColor2Check()
    {
        bm.OrangeCheck();
    }
    public override bool IfSideColorsScored()
    {
        if (bm.greenScored || bm.orangeScored)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class redButton : ColorButton
{

    public override void Start()
    {
        base.Start();
        mainNote = "redNote";
        sideNote1 = "orangeNote";
        sideNote2 = "purpleNote";
        colorMat = bm.redMat;
        colorLine = bm.redLine;

        //button.onClick.AddListener(() =>
        //{
        //    ButtonFonction(bm.redPressed,bm.orangeScored,bm.purpleScored,bm.redActive);
        //});
    }
    //public override void OnPointerDown(PointerEventData eventData)
    //{
    //    ButtonFonction(bm.redPressed, bm.orangeScored, bm.purpleScored, bm.redActive);
    //}
    public override void OnTriggerEnter(Collider col)
    {
        whenNoteIsNearby(col,ref bm.orangeScored,ref bm.purpleScored,ref bm.orangeActive);
    }
    public override void OnTriggerExit(Collider col)
    {
        whenNoteExits(col);
    }
    public override void SideColor1Check()
    {
        bm.OrangeCheck();
    }
    public override void SideColor2Check()
    {
        bm.PurpleCheck();
    }
    public override bool IfSideColorsScored()
    {
        if (bm.orangeScored || bm.purpleScored)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}
